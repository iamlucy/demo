package com.example.android.documentviewerdemo.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.*
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.example.android.documentviewerdemo.R
import com.example.android.documentviewerdemo.adapter.ImageViewerAdapter
import com.example.android.documentviewerdemo.databinding.ViewerImageBinding

class ImageViewerFragment : Fragment(), AdapterView.OnItemSelectedListener,
    ViewSwitcher.ViewFactory {

    private lateinit var imageSwitcher: ImageSwitcher
    private lateinit var gallery: Gallery
    private val id_image = intArrayOf(
        R.drawable.duffy1, R.drawable.duffy2,
        R.drawable.linabell
    )
    private var myAdapter: ImageViewerAdapter? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = DataBindingUtil.inflate<ViewerImageBinding>(inflater,R.layout.viewer_image,container,false
        ).apply {
            lifecycleOwner = viewLifecycleOwner
        }
        gallery = binding.idGallery
        imageSwitcher = binding.idImageSwitcher
        myAdapter = ImageViewerAdapter(id_image, requireContext())
        imageSwitcher!!.setFactory(this)
        gallery!!.onItemSelectedListener = this
        //设置淡入淡出效果
        imageSwitcher!!.inAnimation = AnimationUtils.loadAnimation(requireContext(), android.R.anim.fade_in)
        imageSwitcher!!.outAnimation = AnimationUtils.loadAnimation(requireContext(), android.R.anim.fade_out)
        gallery!!.adapter = myAdapter
        //一定不要忘记 设置gallery的初始位置为中间即可
        gallery!!.setSelection(id_image.size * 100)
        return binding.root
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        imageSwitcher?.setBackgroundResource(id_image[position%id_image.size])
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
        TODO("Not yet implemented")
    }

    override fun makeView(): View {
        val image = ImageView(requireContext())
        image.scaleType = ImageView.ScaleType.FIT_CENTER
        image.layoutParams= FrameLayout.LayoutParams(400,400)
        return image
    }
}