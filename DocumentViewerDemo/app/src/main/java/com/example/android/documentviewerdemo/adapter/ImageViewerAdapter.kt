package com.example.android.documentviewerdemo.adapter

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView.ScaleType

import android.widget.Gallery
import android.widget.ImageView


class ImageViewerAdapter(id_image:IntArray, context: Context): BaseAdapter()  {
    private var id_image: IntArray?=null
    private var context: Context? = null

    init {
        this.context = context
        this.id_image = id_image
    }

    override fun getCount(): Int {
        return Integer.MAX_VALUE
    }

    override fun getItem(position: Int): Int? {
        return id_image?.get(position% id_image!!.size)
    }

    override fun getItemId(position: Int): Long {
        return (position% id_image!!.size).toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val imageView = ImageView(context)
        id_image?.get(position % id_image!!.size)?.let { imageView.setBackgroundResource(it) }
        imageView.layoutParams = Gallery.LayoutParams(250, 200)
        imageView.scaleType = ScaleType.FIT_XY
        return imageView
    }
}